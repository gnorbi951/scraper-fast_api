from functools import lru_cache

from pydantic.env_settings import BaseSettings
from pydantic.fields import Field


class DBSettings(BaseSettings):
    mongo_db_host: str = Field(default="localhost", env="KORMANY_SCRAPER_DB_HOST")
    mongo_db_port: int = Field(default="27017", env="KORMANY_SCRAPER_DB_PORT")
    mongo_db_scraped_data_ttl_days: int = Field(default=120, env="SCRAPED_DATA_TTL")
    mongo_db_user_name: str = Field(
        default="kormany_user", env="KORMANY_SCRAPER_DB_USER_NAME"
    )
    mongo_db_password: str = Field(default="", env="KORMANY_SCRAPER_DB_PASSWORD")
    production: bool = Field(default=True, env="KORMANY_SCRAPER_DB_PRODUCTION")


@lru_cache(maxsize=1)
def get_db_settings() -> DBSettings:
    return DBSettings()
