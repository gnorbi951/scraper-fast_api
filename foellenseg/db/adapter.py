from functools import lru_cache

from pymongo import MongoClient

from foellenseg.db.config import get_db_settings

config = get_db_settings()
ISO_DATE_END_INDEX = 10


def _get_first_plot_from_collection_by_date(collection, date):
    date_to_match = date[:ISO_DATE_END_INDEX]
    result = list(
        collection.find({"date_inserted": {"$regex": f"^{date_to_match}"}})
        .sort("_id", -1)
        .limit(1)
    )
    if result:
        del result[0]["_id"]
    else:
        result = None
    return result


class MongoAdapter:
    def __init__(self, database: str):
        if config.production:
            connection_par = (
                f"mongodb+srv://{config.mongo_db_user_name}:{config.mongo_db_password}@{config.mongo_db_host}"
                "/?retryWrites=true&w=majority"
            )
        else:
            connection_par = f"mongodb://{config.mongo_db_host}:{config.mongo_db_port}"
        self.cluster = MongoClient(connection_par)
        self._scraped_data = self.cluster[database]["scraped_data"]
        self._top10_plot = self.cluster[database]["top10_plot"]
        self._spaghetti_plot = self.cluster[database]["spaghetti_plot"]
        self._top_7_1_day_plot = self.cluster[database]["top_5_1_day_plot"]
        self._top_20_1_month_plot = self.cluster[database]["top_20_1_month_plot"]
        self._live_scraped_data = self.cluster[database]["live_scraped_data"]
        self._live_plot_data = self.cluster[database]["live_plot_data"]

    def get_top10_plot_data(self):
        return list(self._top10_plot.find({}).sort("date_inserted", -1).limit(1))

    def get_spaghetti_plot_data(self):
        return list(self._spaghetti_plot.find({}).sort("date_inserted", -1).limit(1))

    def get_top_7_1_day_plot_data(self):
        return list(self._top_7_1_day_plot.find({}).sort("date_inserted", -1).limit(1))

    def get_top_20_1_month_plot_data(self):
        return list(self._top_20_1_month_plot.find({}).sort("date_inserted", -1).limit(1))

    def get_top_7_1_day_plot_data_by_date(self, date: str):
        return _get_first_plot_from_collection_by_date(self._top_7_1_day_plot, date)

    def get_top10_plot_data_by_date(self, date: str):
        return _get_first_plot_from_collection_by_date(self._top10_plot, date)

    def get_spaghetti_plot_data_by_date(self, date: str):
        return _get_first_plot_from_collection_by_date(self._spaghetti_plot, date)

    def get_top_20_1_month_plot_data_by_date(self, date: str):
        return _get_first_plot_from_collection_by_date(self._top_20_1_month_plot, date)

    def get_live_plot(self):
        return list(self._live_plot_data.find({}).sort("date_inserted", -1).limit(1))


@lru_cache(maxsize=1)
def get_mongo_adapter() -> MongoAdapter:
    return MongoAdapter("kormany_scraper_db")
