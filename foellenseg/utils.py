import pytz
from datetime import datetime


tz = pytz.timezone('Europe/Budapest')


def get_datetime_now() -> datetime:
    return datetime.now(tz=tz).replace(microsecond=0)


def get_iso_date_as_string() -> str:
    return datetime.now(tz=tz).isoformat()


def remove_id_key(func):
    def wrapper(*args, **kwargs):
        # Check if the first argument is a dictionary or list of dictionaries
        if args:
            if isinstance(args[0], dict):
                # Remove '_id' key if present in the dictionary
                if '_id' in args[0]:
                    del args[0]['_id']
            elif isinstance(args[0], list):
                # Remove '_id' key from each dictionary in the list
                for i, d in enumerate(args[0]):
                    if '_id' in d:
                        del args[0][i]['_id']
        # Call the original function with modified arguments
        return func(*args, **kwargs)
    return wrapper
