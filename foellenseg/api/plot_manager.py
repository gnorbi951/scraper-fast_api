from datetime import datetime
from functools import lru_cache

from foellenseg.db.adapter import get_mongo_adapter


class PlotManager:
    def __init__(self):
        self.live_plot = get_mongo_adapter().get_live_plot()
        self.top_20_1_month_plot_data = get_mongo_adapter().get_top_20_1_month_plot_data()
        self.top_7_1_day_plot_data = get_mongo_adapter().get_top_7_1_day_plot_data()
        self.top10_plot_data = get_mongo_adapter().get_top10_plot_data()
        self.spaghetti_plot_data = get_mongo_adapter().get_spaghetti_plot_data()

    def refresh_plots(self):
        minute = datetime.now().time().minute
        refresh_minutes = [5, 6, 7]
        # TODO: Update logic to watch if plots are actually updated.
        if minute in refresh_minutes:
            self.live_plot = get_mongo_adapter().get_live_plot()
            self.top_20_1_month_plot_data = get_mongo_adapter().get_top_20_1_month_plot_data()
            self.top_7_1_day_plot_data = get_mongo_adapter().get_top_7_1_day_plot_data()
            self.top10_plot_data = get_mongo_adapter().get_top10_plot_data()
            self.spaghetti_plot_data = get_mongo_adapter().get_spaghetti_plot_data()


# Exists as singleton
@lru_cache(maxsize=1)
def get_plot_manager() -> PlotManager:
    return PlotManager()
