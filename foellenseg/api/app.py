from pydantic import BaseModel
from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every
from starlette.middleware.cors import CORSMiddleware

from foellenseg.api.config import Settings
from foellenseg.api.handlers import routes
from foellenseg.api.plot_manager import get_plot_manager


def create_app() -> FastAPI:
    api_settings = Settings()
    _app = FastAPI(version="0.0.1", openapi_url=api_settings.openapi_url)
    _app.include_router(routes, prefix="/scraper")

    @_app.get("/ping", response_model=PingResponse)
    async def ping():
        return {"success": True}

    @_app.on_event("startup")
    @repeat_every(seconds=60)
    def refresh_plots():
        plot_manager = get_plot_manager()
        plot_manager.refresh_plots()

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["GET"],
        allow_headers=["*"],
    )

    return _app


class PingResponse(BaseModel):
    success: bool
