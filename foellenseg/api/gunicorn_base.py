import gunicorn.app.base

from foellenseg.api.config import get_api_settings

config = get_api_settings()


class StandaloneApplication(gunicorn.app.base.BaseApplication):
    def __init__(self, app):
        self.options = {
            "bind": f"{config.api_host}:{config.api_port}",
            "sendfile": False,
            "workers": config.api_workers,
            "max_requests": config.api_max_requests,
            "max_requests_jitter": config.api_max_requests_jitter,
            "worker_class": "uvicorn.workers.UvicornWorker",
            "keepalive": 65,
        }
        self.application = app
        super().__init__()

    def init(self, parser, opts, args):
        pass

    def load_config(self):
        for key, value in self.options.items():
            self.cfg.set(key, value)

    def load(self):
        return self.application
