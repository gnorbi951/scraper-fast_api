from functools import lru_cache

from pydantic import BaseSettings, Field


class ApiSettings(BaseSettings):
    api_host: str = Field(default="0.0.0.0", env="FOELLENSEG_API_HOST")
    api_port: int = Field(default=8080, env="FOELLENSEG_API_PORT")
    api_workers: int = Field(default=1, env="FOELLENSEG_API_WORKERS")
    api_max_requests: int = Field(default=1000, env="FOELLENSEG_API_MAX_REQUESTS")
    api_max_requests_jitter: int = Field(
        default=1000, env="KORMANY_SCRAPER_API_MAX_REQUESTS_JITTER"
    )


# Singleton
@lru_cache(maxsize=1)
def get_api_settings() -> ApiSettings:
    return ApiSettings()


class Settings(BaseSettings):
    openapi_url: str = ""
