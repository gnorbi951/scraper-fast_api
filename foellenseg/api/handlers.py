import csv
from datetime import datetime
from typing import Any, List
from fastapi import APIRouter, Request

from foellenseg.api.plot_manager import get_plot_manager
from foellenseg.db.adapter import get_mongo_adapter
from foellenseg.models import ScrapingKeyWords, RateLimiter

routes = APIRouter()
historic_data_rate_limiter = RateLimiter(limit=500, interval_sec=60 * 5)


@routes.get("/plots", response_model=Any)
def get_plots(req: Request):
    query_param = req.query_params.get("plot")
    plot_manager = get_plot_manager()
    if query_param == "weekly_top_10":
        resp = plot_manager.top10_plot_data
    elif query_param == "top_7_1_day_plot":
        resp = plot_manager.top_7_1_day_plot_data
    elif query_param == "top_20_1_month_plot":
        resp = plot_manager.top_20_1_month_plot_data
    elif query_param == "live":
        resp = plot_manager.live_plot
    else:
        resp = plot_manager.spaghetti_plot_data

    # TODO: Decorator
    if resp[0].get("_id") is not None:
        del resp[0]["_id"]  # _id can not be json serialized
    return resp[0]


@routes.get("/plots-by-date", response_model=Any)
def get_plots_by_date(req: Request):
    if historic_data_rate_limiter() == "overload":
        return None
    query_param = req.query_params.get("date")
    if query_param is None:
        return None
    adapter = get_mongo_adapter()
    return {
        "top7": adapter.get_top_7_1_day_plot_data_by_date(query_param),
        "top10": adapter.get_top10_plot_data_by_date(query_param),
        "spaghetti": adapter.get_spaghetti_plot_data_by_date(query_param),
        "monthly": adapter.get_top_20_1_month_plot_data_by_date(query_param),
    }


@routes.get("/heartbeat")
def log_data(request: Request):
    date_string = datetime.now().strftime('%Y-%m')
    timestamp = datetime.now().replace(microsecond=0)
    ua = request.headers.get('User-Agent')
    ia = request.client.host
    page = request.query_params.get("page")
    with open(f'../heartbeat_{date_string}.csv', mode='a', newline='') as log_file:
        log_writer = csv.writer(log_file)
        log_writer.writerow([ua, ia, page, timestamp])
    return {"message": "Beep boop"}


@routes.get("/keywords", response_model=List[str])
def get_keywords():
    return ScrapingKeyWords().get_frontend_topics()


@routes.get("/keywordsv2")
def get_keywords_v2():
    return ScrapingKeyWords().fields