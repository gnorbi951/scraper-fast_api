import time
from typing import Dict, Set, Optional


class ScrapingKeyWords:
    def __init__(self):
        # {'main_keyword': {'words', 'to', 'look', 'for'}}
        # Instead of set of str, you can use regex string
        self.fields: Dict[str, Set[str]] = {
            "Jakab Péter": {"jakab"},
            "Gyurcsány": {"gyurcsány", "fletó"},
            "Dobrev Klára": {"dobrev", "gyurcsányné"},
            "Hadházy Ákos": {"hadházy"},
            "Fekete-Győr András": {"fegyőr", "fekete-győr"},
            "Donáth Anna": {"donáth"},
            "Cseh Kata": {"cseh kata"},
            "Karácsony": {"karácsony", "karigeri"},
            "Szabó Tímea": {"szabó tímea"},
            "Joe Biden": {"biden", "amerika elnök", "amerikai elnök"},
            "Sargentini": {"sargentini"},
            "Von der Leyen": {"von der leyen"},
            "Angela Merkel": {"merkel"},
            "Jean-Claude Juncker": {"juncker"},
            "Greta Thunberg": {"greta thunberg"},
            "Emanuel Macron": {"macron"},
            "Vona Gábor": {"vona "},
            "Konok Péter": {"konok"},
            "Márki-Zay Péter": {
                "mzp",
                "mini feri",
                "mini-feri",
                "márki-zay",
                "gombaszakértő",
                "maki-zay",
                "makizaj",
            },
            "Zelenszkij": {"zelenszkij", "ukrán elnök"},
            "Niedermüller Péter": {"niedermüller"},
            "Schiffer András": {"schiffer"},
            "Soros György": {"soros"},
            "Tordai Bence": {"tordai"},
            "Ungár Péter": {"ungár"},
            "Puzsér Róbert": {"puzsér"},

            "Migráns": {"migráns", "menekült"},
            "Baloldal": {
                "baloldal",
                "bal oldal",
                "bal-oldal",
                "balliberális",
                "bal-liberális",
                "bal liberális",
                "balos",  # vadhajtasok helyesiras
            },
            "Oltásellenes": {"oltásellenes", "oltás ellenes", "oltás-ellenes"},
            "Liberális": {"liberális", "libsi", "lipsi", "libis"},
            "Melegek": {
                "lmbt",
                "lgbt",
                "pride",
                "leszbikus",
                "meleg",
                "homoszexuális",
                "pedofil",
                "nem váltó",
                "nem-váltó",
            },
            "Légitársaságok": {"légitársaság", "wizz air", "wizzair", "ryanair", "klm"},
            "Arabok": {"arab", "iszlám", "muszlim"},
            "NGO": {
                "soros szervezet",
                "civil szervezet",
                "ngo ",
                " ngo",
                "ngo-",
                "transparency",
            },
            "Tanárok": {"tanár", "pedagógus", "oktató"},
            "Orvosok": {"orvos", ' OMK', 'OMK ', 'orvosi kamara', 'gyógyász', 'egészségügy'},
            "Nyugat": {"nyugat"},
            "Civil": {"civil"},
            "Zöld": {"zöld", "környezetvédő"},
            "Kommunista": {"kommunista", "komcsi"},
            "Gender": {"gender"},
            "Egyetemisták": {"egyetem", "ceu", "szfe"},
            "Szankciók": {"szankció"},

            "Anglia": {"angol ", " angol", "anglia", "brit"},
            "Ukrajna": {"ukrajna", "ukrán"},
            "Románia": {"román"},
            "Németország": {"német"},
            "Franciaország": {"francia"},
            "USA": {"amerika", " usa "},
            "Hollandia": {"holland"},
            "Szlovákia": {"szlovák"},
            "Ausztria": {"ausztria", "osztrák"},
            "Brüsszel": {"brüsszel"},
            "Svédország": {"svéd"},
            "Finnország": {"finn"},
            "NATO": {"nato-", "nato ", " nato"},

            "Ellenzék": {
                "ellenzék",
                "ellenzéki összefogás",
                "moslék koalíció",
                "szivárvány koalíció",
            },
            "Momentum": {"momentum"},
            "MSZP": {"mszp", "magyar szocialista párt"},
            "Jobbik": {"jobbik"},
            "Dk": {" dk", "dk ", "dk-", "demokratikus koalíció"},
            "LMP": {"lmp", "lehet más a politika"},
            "Kutyapárt": {"kutyapárt", "kétfarkú", "mkkp"},
            "444": {"444", "négynégynégy"},
            "Telex": {"telex", "teleksz"},
        }

    def get_main_topics(self) -> Dict[str, int]:
        return dict.fromkeys(self.fields, 0)

    def get_frontend_topics(self) -> list[str]:
        return list(dict.fromkeys(self.fields, 0).keys())


class ScrapingKeywordsResponseModel:
    def __init__(self):
        keywords = ScrapingKeyWords().get_main_topics()
        for key, value in keywords.items():
            setattr(self, key, value)


class RateLimiter:
    def __init__(self, limit: int, interval_sec: int):
        self.limit = limit
        self.interval = interval_sec
        self.timestamps = []

    def __call__(self) -> Optional[str]:
        now = time.time()
        self.timestamps = [ts for ts in self.timestamps if now - ts <= self.interval]
        self.timestamps.append(now)

        if len(self.timestamps) > self.limit:
            return "overload"
