import click

from foellenseg.api.app import create_app
from foellenseg.api.config import get_api_settings
from foellenseg.api.gunicorn_base import StandaloneApplication


@click.group()
def main():
    pass


config = get_api_settings()


@main.command()
@click.option("--wsgi", type=click.Choice(["gunicorn"]), default="gunicorn")
def run_api(wsgi):
    app = create_app()

    if wsgi == "gunicorn":
        StandaloneApplication(app).run()


# @main.command()
# @click.option("-o", "--output-file", type=str, default="openapi.yaml")
# def openapi(output_file):
#     import yaml  # pylint: disable=import-outside-toplevel
#
#     openapi_json = create_app().openapi()
#     with open(output_file, "w", encoding="utf-8") as file:
#         file.write(yaml.dump(openapi_json))


if __name__ == "__main__":
    main()
