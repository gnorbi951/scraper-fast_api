from setuptools import setup, find_packages


requirements = [
    'fastapi',
    'fastapi_utils',
    'gunicorn',
    'uvicorn',
    'requests',
    'pymongo',
]

test_requirements = [
    'pytest',
    'pytest-cov',
    'pylint',
]

dev_requirements = ['pre-commit']

setup(
    author='Norbert Gocze',
    author_email='gnorbi951@gmail.com',
    python_requires='~=3.10',
    description="Provides webserver for scraper.",
    long_description_content_type='text/markdown',
    entry_points={
        'console_scripts': [
            'foellenseg=foellenseg.cli:main',
        ],
    },
    include_package_data=True,
    keywords='foellenseg',
    name='foellenseg',
    packages=find_packages(),
    install_requires=requirements,
    extras_require={
        'dev': test_requirements + dev_requirements,
        'test': test_requirements,
    },
    test_suite='tests',
    tests_require=test_requirements,
)